package com.dkarmazi.android.myapplication;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

public class CustomModel extends BaseObservable {
    private String name;
    private String desc;

    public CustomModel(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // This field is observable, desc will be added to BR object
    @Bindable
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
        // the following line might cause problems in Android Studio
        // clean and rebuild and run despite the errors
        notifyPropertyChanged(com.dkarmazi.android.myapplication.BR.desc);
    }
}
