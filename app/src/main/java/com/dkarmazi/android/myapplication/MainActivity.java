package com.dkarmazi.android.myapplication;

import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.dkarmazi.android.myapplication.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private CustomModel customModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // this class is generated based on layout name: activity_main
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        customModel = new CustomModel("Click me to change textview on the right", "ohoho");
        binding.setHandlers(this);
        binding.setMymodel(customModel);
    }

    public void onNameClicked(View view) {
        Log.d(TAG, "onNameClicked() called with: " + "view = [" + view + "]");

        if(customModel.getDesc().equals("ohoho")) {
            customModel.setDesc("hahaha");
        } else {
            customModel.setDesc("ohoho");
        }

    }

    public void onDescClicked(View view) {
        Log.d(TAG, "onDescClicked() called with: " + "view = [" + view + "]");
    }
}
